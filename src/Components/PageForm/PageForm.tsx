import { useMutation } from '@apollo/react-hooks'
import { Button, Card, Form, Icon, Input, notification, Typography } from 'antd'
import gql from 'graphql-tag'
import React, { Dispatch, SetStateAction, useState } from 'react'

import { Page } from '../Page/Page'

const { TextArea } = Input

interface Props {
    values: { [key: string]: string }
    sendForm: (values: Props['values']) => Promise<void>
}

export const ICCItems = [
    {
        text: 'Категория',
        path: 'category',
        type: Input,
    },
    {
        text: 'Телефон',
        path: 'phone',
        type: Input,
    },
    {
        text: 'Проблема, изложенная клиентом',
        path: 'problem',
    },
    {
        text: 'Документы, имеющиеся на руках у клиента',
        path: 'documents',
    },
    {
        text: 'Дата и время первой встречи',
        path: 'data',
        type: Input,
    },
    {
        text: 'Источник получения информации о консультации',
        path: 'source',
        type: Input,
    },
    {
        text: 'Дата принятия заявки',
        path: 'applicationData',
        type: Input,
    },
    {
        text: 'Диспетчер, принявший заявку',
        path: 'dispatcher',
        type: Input,
    },
    {
        text: 'Отраслевая принадлежность',
        path: 'field',
        type: Input,
    },
    {
        text: 'Консультант',
        path: 'consultant',
        type: Input,
    },
    {
        text: 'Специалист',
        path: 'specialist',
        type: Input,
    },
]

const FormView = (props: Props) => {
    const [value, setValue]: [
        { [key: string]: string },
        { [key: string]: Dispatch<SetStateAction<string>> }
    ] = [props.values || {}, {}]

    ICCItems.forEach(item => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        ;[value[item.path], setValue[item.path]] = useState(props.values[item.path])
    })

    return (
        <Form>
            {ICCItems.map(item => (
                <Form.Item key={item.text} label={item.text}>
                    {item.type ? (
                        <item.type
                            value={value[item.path]}
                            onChange={e => setValue[item.path](e.target.value)}
                        />
                    ) : (
                        <TextArea
                            autosize={true}
                            value={value[item.path]}
                            onChange={e => setValue[item.path](e.target.value)}
                        />
                    )}
                </Form.Item>
            ))}
            <Button
                type="primary"
                htmlType="submit"
                onClick={async () => {
                    if (ICCItems.reduce((acc, cur) => acc && Boolean(value[cur.path]), true)) {
                        await props.sendForm(value)
                        // ICCItems.forEach(item => {
                        //     setValue[item.path]('')
                        // })
                    } else {
                        notification.open({
                            message: 'Не все поля заполнены!',
                            icon: <Icon type="exclamation-circle" style={{ color: '#f00' }} />,
                        })
                    }
                }}
            >
                Отправить
            </Button>
        </Form>
    )
}

export const ICCForm = Form.create()(FormView) as any

const START_CASE = gql`
    mutation CreateCase($cat: name) {
        insert_cases(objects: [{ case_status: "open", category: $cat }]) {
            affected_rows
            returning {
                case_id
            }
        }
    }
`

const INIT_CASE = gql`
    mutation InitCase($cid: Int, $sid: Int, $tid: Int, $data: json) {
        insert_case_student_relations(objects: [{ case_id: $cid, student_id: $sid }]) {
            affected_rows
        }
        insert_case_teachers_relations(objects: [{ case_id: $cid, teacher_id: $tid }]) {
            affected_rows
        }
        insert_cases_feed(
            objects: [{ case_id: $cid, case_status: "icc_review", event_data: $data }]
        ) {
            affected_rows
        }
    }
`

const ICCPage = () => {
    const [startCase] = useMutation(START_CASE)
    const [initCase] = useMutation(INIT_CASE)

    return (
        <Page>
            <Typography.Title>Регистрация обращения</Typography.Title>
            <Card>
                <ICCForm
                    values={{}}
                    sendForm={async (values: any) => {
                        notification.open({
                            message: 'Дело создается',
                            icon: <Icon type="loading" />,
                        })
                        const res = await startCase({
                            variables: { cat: values.category },
                        })
                        const cid = res.data.insert_cases.returning[0].case_id

                        await initCase({
                            variables: {
                                cid,
                                sid: values.consultant,
                                tid: values.specialist,
                                data: JSON.stringify({ values }),
                            },
                        })
                        notification.destroy()
                        notification.open({
                            message: 'Дело создано',
                            icon: <Icon type="check-circle" style={{ color: '#108ee9' }} />,
                        })
                    }}
                />
            </Card>
        </Page>
    )
}

export default ICCPage
