import { useCallback, useEffect, useState } from 'react'

export type State = {
    userRoles: ('student' | 'tutor' | 'prof')[]
    studentId: number | undefined
    teacherId: number | undefined
    userId: number | undefined
}

let state = {
    userRoles: [],
    studentId: undefined,
    teacherId: undefined,
    userId: undefined,
}

type Listener = () => void

const subs: Array<Listener> = []

export const getState = (): State => state
export const setState = (patch: State) => {
    if (Object.keys(patch).length === 0) {
        localStorage.removeItem('jc-state')
        state = { userRoles: [] } as any
    } else {
        state = Object.assign(state, patch)
    }
    subs.forEach(sub => sub())
    localStorage.setItem('jc-state', JSON.stringify(state))
}
export const sub = (cb: Listener) => {
    subs.push(cb)

    return () => {
        if (subs.includes(cb)) {
            subs.splice(subs.indexOf(cb))
        }
    }
}

export const useAppState = () => {
    const [, updateState] = useState()
    const forceUpdate = useCallback(() => updateState({}), [])

    useEffect(() => sub(forceUpdate), [forceUpdate])

    return getState()
}

export const roleMatch = (required: string[], userRoles: string[]) => {
    for (const role of userRoles) {
        if (required.includes(role)) {
            return true
        }
    }

    return false
}

try {
    const lState = localStorage.getItem('jc-state')

    if (lState) {
        setState(JSON.parse(lState))
    }
} catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
}
