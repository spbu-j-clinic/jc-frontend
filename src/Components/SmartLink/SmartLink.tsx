import React from 'react'

export const SmartLink: React.FC<{ link: string }> = props => {
    if (props.link.includes('docs.google')) {
        return (
            <iframe
                src={props.link}
                frameBorder={0}
                style={{ height: '40vh', width: '100%', border: '1px solid #eee' }}
            />
        )
    } else {
        return (
            <a href={props.link} target="_blank" rel="noopener noreferrer">
                {props.children || 'Скачать'}
            </a>
        )
    }
}
