import React from 'react'

import styles from './Page.less'

export const Page: React.FC = props => <main className={styles.page}>{props.children}</main>
