import { Collapse, PageHeader } from 'antd'
import React from 'react'
const { Panel } = Collapse

type Props = { seed: string }
type State = {
    error: null | unknown
    errorInfo: null | React.ErrorInfo
    oldSeed: string
}
export class ErrorBoundary extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = { error: null, errorInfo: null, oldSeed: '' }
    }

    componentDidCatch(error: unknown, errorInfo: React.ErrorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
            error,
            errorInfo,
        })
        // You can also log error messages to an error reporting service here
    }

    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.seed !== state.oldSeed) {
            return { error: null, errorInfo: null, oldSeed: props.seed }
        }

        return null
    }

    render() {
        if (this.state.errorInfo) {
            // Error path
            return (
                <div>
                    <PageHeader title={Error} />
                    <Collapse>
                        <Panel
                            header={String(this.state.error)}
                            style={{ whiteSpace: 'pre-wrap' }}
                            key="error"
                        >
                            {this.state.errorInfo.componentStack}
                        </Panel>
                        {this.state.error instanceof Error && (
                            <Panel
                                header={`${this.state.error.name} stack`}
                                style={{ whiteSpace: 'pre-wrap' }}
                                key="stack"
                            >
                                {this.state.error.stack}
                            </Panel>
                        )}
                    </Collapse>
                </div>
            )
        }

        // Normally, just render children
        return this.props.children
    }
}
