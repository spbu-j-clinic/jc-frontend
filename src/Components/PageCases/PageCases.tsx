import { useQuery } from '@apollo/react-hooks'
import { Alert, List, Typography } from 'antd'
import gql from 'graphql-tag'
import React from 'react'
import { Link } from 'react-router5'

import { byNumber } from '../../utils/sortBy'
import { Page } from '../Page/Page'
import { getState } from '../state'

const cases = gql`
    query MyQuery($studentId: Int!, $teacherId: Int!) {
        case_teachers_relations(where: { teacher_id: { _eq: $teacherId } }) {
            case_id
        }
        case_student_relations(where: { student_id: { _eq: $studentId } }) {
            case_id
        }
    }
`

type IdEntry = {
    case_id: number
}

type Cases = {
    case_teachers_relations: IdEntry[]
    case_student_relations: IdEntry[]
}

const PageMe = () => {
    const { loading, error, data } = useQuery<Cases>(cases, {
        variables: { studentId: getState().studentId || 0, teacherId: getState().teacherId || 0 },
    })

    const concat = data ? data.case_student_relations.concat(data.case_teachers_relations) : []
    const ids = [...new Set(concat.map(item => item.case_id))]

    ids.sort(byNumber)

    return (
        <Page>
            <Typography.Title>Мои дела</Typography.Title>

            {error && <Alert type="error" message={JSON.stringify(error)} />}
            {data && (
                <List bordered={true} loading={loading} style={{ background: 'white' }}>
                    {ids.map(id => (
                        <List.Item key={id}>
                            <Link routeName={'case'} routeParams={{ id }}>
                                Дело {id}
                            </Link>
                        </List.Item>
                    ))}
                </List>
            )}
        </Page>
    )
}

export default PageMe
