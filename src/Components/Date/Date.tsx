import { Tooltip } from 'antd'
import day from 'dayjs'
import React from 'react'

export const DateView: React.FC<{ date: string | number | Date }> = props => {
    return (
        <Tooltip title={day(props.date).format('DD MMMM YYYY')}>
            {day(props.date).from(day())}
        </Tooltip>
    )
}
