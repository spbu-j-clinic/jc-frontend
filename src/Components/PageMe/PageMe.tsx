import { useQuery } from '@apollo/react-hooks'
import { Alert, Card, Descriptions, Table, Typography } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import day from 'dayjs'
import gql from 'graphql-tag'
import React from 'react'

import { byNumber, lexical, sortBy } from '../../utils/sortBy'
import { useSortedColumns } from '../../utils/useSortedColumns'
import { Page } from '../Page/Page'
import { getState } from '../state'

const ipp = gql`
    query ipp($id: Int!) {
        points_feed(where: { student_id: { _eq: $id } }) {
            points
            reason
            date_time
            event_id
            teacher {
                site_user {
                    name
                }
            }
        }
        students(where: { student_id: { _eq: $id } }) {
            site_user {
                name
            }
            points
            functional_group
        }
    }
`

type PointsEntry = {
    points: number
    reason: string
    date: string
    event_id: number
    teacher: {
        site_user: {
            name: string
        }
    }
}

type IPP = {
    points_feed: PointsEntry[]
    students: {
        site_user: {
            name: string
        }
        points: number
        functional_group: string
    }[]
}

const dateRender = (value: string) => day(value).format('DD MMMM YYYY')

const teacherRender = (value: PointsEntry['teacher']) => value.site_user.name

const columns: ColumnProps<PointsEntry>[] = [
    {
        title: 'Количество',
        dataIndex: 'points',
        key: 'points',
        sorter: sortBy('points', byNumber),
    },
    {
        title: 'За что',
        dataIndex: 'reason',
        key: 'reason',
        sorter: sortBy('reason', lexical),
    },
    {
        title: 'Дата',
        dataIndex: 'date',
        key: 'date',
        sorter: sortBy('date', lexical),
        render: dateRender,
    },
    {
        title: 'Преподаватель',
        dataIndex: 'teacher',
        key: 'teacher',
        sorter: (a, b) => a.teacher.site_user.name.localeCompare(b.teacher.site_user.name),
        render: teacherRender,
    },
]

const PageMe = () => {
    const { loading, error, data } = useQuery<IPP>(ipp, { variables: { id: getState().studentId } })

    const [sorted, handleChange] = useSortedColumns(columns)

    const points = data ? data.points_feed.reduce((acc, item) => acc + item.points, 0) : 0

    return (
        <Page>
            <Typography.Title>ИПП</Typography.Title>
            <Card loading={loading}>
                {error && <Alert type="error" message={JSON.stringify(error)} />}
                {data && (
                    <React.Fragment>
                        <Typography.Title level={3}>Информация</Typography.Title>
                        <Descriptions bordered={true} layout="horizontal" column={1}>
                            <Descriptions.Item label="Имя">
                                {data.students[0].site_user.name}
                            </Descriptions.Item>
                            <Descriptions.Item label="Группа">
                                {data.students[0].functional_group || 'Не выбрана'}
                            </Descriptions.Item>
                            <Descriptions.Item label="Всего баллов">{points}</Descriptions.Item>
                        </Descriptions>
                        <br></br>
                        <Typography.Title level={3}>Баллы</Typography.Title>
                        <Table
                            columns={sorted}
                            onChange={handleChange}
                            dataSource={data.points_feed}
                            pagination={false}
                            rowKey="event_id"
                            bordered={true}
                        />
                    </React.Fragment>
                )}
            </Card>
        </Page>
    )
}

export default PageMe
