import { useQuery } from '@apollo/react-hooks'
import { Alert, Card, Table, Typography } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import gql from 'graphql-tag'
import React from 'react'

import { byNumber, sortBy } from '../../utils/sortBy'
import { useSortedColumns } from '../../utils/useSortedColumns'
import { Page } from '../Page/Page'

const ipp = gql`
    query showStudents {
        students {
            site_user {
                name
            }
            points
        }
    }
`

type Student = {
    site_user: {
        name: string
    }
    points: number
}

type Students = {
    students: Student[]
}

const nameRender = (value: Student['site_user']) => value.name || ''

const columns: ColumnProps<Student>[] = [
    {
        title: 'Имя',
        dataIndex: 'site_user',
        key: 'site_user',
        sorter: (a, b) => a.site_user.name.localeCompare(b.site_user.name),
        render: nameRender
    },
    {
        title: 'Баллы',
        dataIndex: 'points',
        key: 'points',
        sorter: sortBy('points', byNumber),
    }
]

const PageStudents = () => {
    const { loading, error, data } = useQuery<Students>(ipp)

    const [sorted, handleChange] = useSortedColumns(columns)

    return (
        <Page>
            <Typography.Title>Список студентов</Typography.Title>
            <Card loading={loading}>
                {error && <Alert type="error" message={JSON.stringify(error)} />}
                {data && (
                    <React.Fragment>
                        <Typography.Title level={3}>Баллы</Typography.Title>
                        <Table
                            columns={sorted}
                            onChange={handleChange}
                            dataSource={data.students}
                            pagination={false}
                            rowKey="event_id"
                            bordered={true}
                        />
                    </React.Fragment>
                )}
            </Card>
        </Page>
    )
}

export default PageStudents
