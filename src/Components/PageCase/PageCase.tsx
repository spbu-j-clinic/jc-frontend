import { useQuery } from '@apollo/react-hooks'
import { Alert, Card, Timeline, Typography } from 'antd'
import gql from 'graphql-tag'
import React from 'react'
import { useRoute } from 'react-router5'

import { EventView } from '../Event/Event'
import { Page } from '../Page/Page'

const getCase = gql`
    query getCase($id: Int) {
        __typename
        cases(where: { case_id: { _eq: $id } }) {
            category
            case_id
        }
        cases_feed(where: { case_id: { _eq: $id } }, order_by: { date_time: desc }) {
            event_data
            case_status
            date_time
            done
            event_id
        }
    }
`

type CaseData = {
    cases: [{ category: string }]
    cases_feed: Array<{
        event_data: any
        case_status: string
        date_time: string
        event_id: number
        done: boolean
    }>
}

const PageCase = () => {
    const r5 = useRoute()
    const case_id = r5.route.params.id

    const { loading, error, data } = useQuery<CaseData>(getCase, {
        variables: { id: case_id },
        pollInterval: 500,
    })

    return (
        <Page>
            <Typography.Title>Дело {r5.route.params.id}</Typography.Title>
            <Timeline>
                {loading && (
                    <Timeline.Item>
                        <Card loading={true} />
                    </Timeline.Item>
                )}
                {error && <Alert type="error" message={JSON.stringify(error)} />}
                {data &&
                    data.cases_feed.map(e => (
                        <Timeline.Item key={e.event_id}>
                            <EventView
                                case_id={case_id}
                                event_id={e.event_id}
                                date={e.date_time}
                                done={e.done}
                                eventData={e.event_data}
                                type={e.case_status}
                                titlePrefix=""
                            />
                        </Timeline.Item>
                    ))}
            </Timeline>
        </Page>
    )
}

export default PageCase
