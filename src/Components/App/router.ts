import React, { lazy } from 'react'
import { useRoute } from 'react-router5'
import { RouteContext } from 'react-router5/types/types'
import createRouter from 'router5'
import browserPlugin from 'router5-plugin-browser'

import PageForm from '../PageForm/PageForm'
import SendPointsForm from '../SendPointsForm/SendPointsForm'

export const routes = [
    {
        name: 'me',
        path: '/',
    },
    {
        name: 'events',
        path: '/events',
    },
    {
        name: 'cases',
        path: '/cases',
    },
    {
        name: 'case',
        path: '/case/:id/',
    },
    {
        name: 'form',
        path: '/form',
    },
    {
        name: 'login',
        path: '/login',
    },
    {
        name: 'sendPointsForm',
        path: '/sendPointsForm',
    },
    {
        name: 'students',
        path: '/students'
    }
] as const

type RouteName = (typeof routes)[number]['name']

export const routeInfo: {
    [K in RouteName]: {
        icon: string
        view: React.ComponentType
        title: string
        roles: Array<'student' | 'tutor' | 'prof'>
    }
} = {
    me: {
        icon: 'idcard',
        view: lazy(() => import(/* webpackChunkName: 'PageMe' */ '../PageMe/PageMe')),
        title: 'ИПП',
        roles: ['student'],
    },
    events: {
        icon: 'notification',
        view: lazy(() => import(/* webpackChunkName: 'PageEvents' */ '../PageEvents/PageEvents')),
        title: 'Оповещения',
        roles: ['student', 'prof', 'tutor'],
    },
    cases: {
        icon: 'snippets',
        view: lazy(() => import(/* webpackChunkName: 'PageCases' */ '../PageCases/PageCases')),
        title: 'Мои дела',
        roles: ['student', 'prof', 'tutor'],
    },
    case: {
        icon: 'snippets',
        view: lazy(() => import(/* webpackChunkName: 'PageCase' */ '../PageCase/PageCase')),
        title: 'Дело',
        roles: [],
    },
    form: {
        icon: 'form',
        view: lazy(() => import(/* webpackChunkName: 'PageForm' */ '../PageForm/PageForm') as any),
        title: 'Новое обращение',
        roles: ['student'],
    },
    login: {
        icon: 'login',
        view: lazy(() => import(/* webpackChunkName: 'PageLogin' */ '../PageLogin/PageLogin')),
        title: 'Вход',
        roles: [],
    },
    sendPointsForm: {
        icon: 'points',
        view: SendPointsForm,
        title: 'Отправить баллы',
        roles: ['prof', 'tutor']
    },
    students: {
        icon: 'team',
        view: lazy(() => import(/* webpackChunkName: 'PageStudents' */ '../PageStudents/PageStudents')),
        title: 'Студенты',
        roles: ['student', 'prof', 'tutor']
    }
}
export const defaultRoute: RouteName = 'events'

export const router = createRouter(routes as any, { defaultRoute })
router.usePlugin(browserPlugin())
router.start() // maybe router.start('/login');

export const useJCRoute = () => {
    return useRoute() as RouteContext & { route: { name: RouteName } }
}
