import { Icon, Layout, Menu } from 'antd'
import React, { Suspense, useState } from 'react'
import { Link } from 'react-router5'

import { ErrorBoundary } from '../ErrorBoundary'
const { Content, Sider } = Layout

import { roleMatch, setState, useAppState } from '../state'
import styles from './Layout.less'
import { routeInfo, routes, useJCRoute } from './router'

export const JCLayout = () => {
    const state = useAppState()
    const [collapsed, setCollapsed] = useState(false)

    const r5 = useJCRoute()
    const View = routeInfo[r5.route.name].view

    if (!state.userId) {
        // Dirty hack as all this fucking codebase is
        setTimeout(() => r5.router.navigate('login'), 100)
    }

    return (
        <Layout>
            <Sider
                collapsible={true}
                collapsed={collapsed}
                onCollapse={setCollapsed}
                theme={'light'}
            >
                <Menu mode="inline" selectedKeys={[r5.route.name]} className={styles.menu}>
                    {routes.map(
                        route =>
                            (roleMatch(routeInfo[route.name].roles, state.userRoles) ||
                                r5.route.name === route.name) && (
                                <Menu.Item key={route.name}>
                                    <Link
                                        routeName={route.name}
                                        key={route.name}
                                        routeParams={r5.route.params}
                                    >
                                        <Icon type={routeInfo[route.name].icon} />
                                        <span>{routeInfo[route.name].title}</span>
                                    </Link>
                                </Menu.Item>
                            )
                    )}
                    {state.userId && (
                        <Menu.Item key={'logout'} onClick={() => setState({} as any)}>
                            <Icon type={'logout'} />
                            Выйти
                        </Menu.Item>
                    )}
                </Menu>
            </Sider>
            <Content>
                <ErrorBoundary seed={r5.route.name}>
                    <Suspense fallback={null}>
                        <View />
                    </Suspense>
                </ErrorBoundary>
            </Content>
        </Layout>
    )
}
