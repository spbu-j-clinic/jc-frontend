import { ApolloProvider } from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'
import React from 'react'
import { RouterProvider } from 'react-router5'

import { ErrorBoundary } from '../ErrorBoundary'
import { JCLayout } from './Layout'
import { router } from './router'

const client = new ApolloClient({
    uri: 'http://192.168.148.21:8080/v1/graphql',
})

export const App = () => {
    return (
        <ErrorBoundary seed={''}>
            <ApolloProvider client={client}>
                <RouterProvider router={router}>
                    <JCLayout />
                </RouterProvider>
            </ApolloProvider>
        </ErrorBoundary>
    )
}
