import { Typography } from 'antd'
import React from 'react'

import { EventView } from '../Event/Event'
import { Page } from '../Page/Page'
import styles from './PageEvents.less'

const events = [
    {
        type: 'interview_plan_request',
        date: Date.now() - 1e6,
        title: 'Дело №23 — ',
        eventData: { done: false },
    },
    {
        type: 'report_request',
        date: Date.now() - 1e8,
        title: 'Дело №11 — ',
        eventData: {
            planLink:
                'https://docs.google.com/document/d/1oYYJkSrc3dqzExSGOsytnCkSAOd2yHS06jXqzWYzo1I/edit',
            done: false,
        },
    },
]

const PageEvents = () => {
    return (
        <Page>
            <Typography.Title>Оповещения</Typography.Title>

            {events.map(e => (
                <div className={styles.item} key={e.type + e.date}>
                    <EventView {...(e as any)} titlePrefix={e.title} />
                </div>
            ))}
        </Page>
    )
}

export default PageEvents
