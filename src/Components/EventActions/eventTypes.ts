import { ReactNode } from 'react'

export type ActionProps = {
    title: string | ReactNode
}
