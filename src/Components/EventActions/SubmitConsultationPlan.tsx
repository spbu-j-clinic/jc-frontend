import { Button, Card, Form, Input, message } from 'antd'
import React, { useState } from 'react'

import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onSubmit: (plan: string, memo: string) => void
}

export const ConsultPlanCard: React.FC<Props> = props => {
    const [plan, setPlan] = useState('')
    const [memo, setMemo] = useState('')

    const handleSubmit = () =>
        plan && memo
            ? props.onSubmit(plan, memo)
            : message.error('Нужно добавить ссылки на план консультации и меморандум')

    return (
        <Card title={props.title}>
            {props.children && <div style={{ marginBottom: '20px' }}>{props.children}</div>}
            <Form>
                <Form.Item label="Ссылка на план консультации" required={true}>
                    <Input value={plan} onChange={e => setPlan(e.target.value)} />
                </Form.Item>
                <Form.Item label="Ссылка на меморандум консультации" required={true}>
                    <Input value={memo} onChange={e => setMemo(e.target.value)} />
                </Form.Item>
                <Button onClick={handleSubmit} type={'primary'}>
                    Оправить на проверку
                </Button>
            </Form>
        </Card>
    )
}
