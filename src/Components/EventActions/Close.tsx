import { Button, Card, Form, Input, message, Radio } from 'antd'
import React, { useState } from 'react'

import { resolutionText } from '../EventData/Report'
import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onSubmit: (report: string, resolution: string) => void
}

export const CloseCard: React.FC<Props> = props => {
    const [report, setReport] = useState('')
    const [resolution, setResolution] = useState('')

    const handleSubmit = () =>
        report && resolution
            ? props.onSubmit(report, resolution)
            : message.error('Нужно добавить ссылку на отчет и резолюцию')

    return (
        <Card title={props.title}>
            {props.children && <div style={{ marginBottom: '20px' }}>{props.children}</div>}
            <Form>
                <Form.Item label="Ссылка на отчет" required={true}>
                    <Input value={report} onChange={e => setReport(e.target.value)} />
                </Form.Item>
                <Form.Item label="Резолюция" required={true}>
                    <Radio.Group value={resolution} onChange={e => setResolution(e.target.value)}>
                        <Radio value="done">{resolutionText.done}</Radio>
                        <Radio value="skip">{resolutionText.skip}</Radio>
                    </Radio.Group>
                </Form.Item>
                <Button onClick={handleSubmit} type={'primary'}>
                    Закрыть дело
                </Button>
            </Form>
        </Card>
    )
}
