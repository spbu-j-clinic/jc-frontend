import { Card } from 'antd'
import React from 'react'

import { ICCForm } from '../PageForm/PageForm'
import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onSubmit: (values: any) => void
    values: any
}

export const ICCCard: React.FC<Props> = props => {
    return (
        <Card title={props.title}>
            {props.children && <div style={{ marginBottom: '20px' }}>{props.children}</div>}
            <ICCForm sendForm={props.onSubmit} values={props.values} />
        </Card>
    )
}
