import { Button, Card, Input, message } from 'antd'
import React from 'react'

import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onAccept: () => void
    onReject: (reason: string) => void
}

export const CheckCard: React.FC<Props> = props => {
    const accept = (
        <Button onClick={props.onAccept} type="primary" key="accept">
            Принять
        </Button>
    )
    const reject = (
        <div style={{ padding: '0 24px' }}>
            <Input.Search
                enterButton={<Button type="danger">Исправить</Button>}
                onSearch={reason =>
                    reason
                        ? props.onReject(reason)
                        : message.error('Нужно написать что надо исправить')
                }
            />
        </div>
    )

    return (
        <Card actions={[accept, reject]} title={props.title}>
            {props.children}
        </Card>
    )
}
