import { Card, Input, message } from 'antd'
import React from 'react'

import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onSubmit: (link: string) => void
}

export const InterviewPlanCard: React.FC<Props> = props => {
    return (
        <Card title={props.title}>
            {props.children && <div style={{ marginBottom: '20px' }}>{props.children}</div>}
            <Input.Search
                enterButton={'Отправить на проверку'}
                placeholder="Ссылка на план интервью"
                onSearch={link =>
                    link
                        ? props.onSubmit(link)
                        : message.error('Нужно добавить ссылку на план интервью')
                }
            />
        </Card>
    )
}
