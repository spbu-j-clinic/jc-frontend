import { Button, Card, Form, Input, message } from 'antd'
import React, { useState } from 'react'

import { ActionProps } from './eventTypes'

export type Props = ActionProps & {
    onSubmit: (link: string, repeat: boolean) => void
}

export const InterviewResultCard: React.FC<Props> = props => {
    const [link, setLink] = useState('')

    const handleSubmit = (repeat: boolean) => () =>
        link
            ? props.onSubmit(link, repeat)
            : message.error('Нужно добавить ссылку на резюме интервью')

    return (
        <Card title={props.title}>
            {props.children && <div style={{ marginBottom: '20px' }}>{props.children}</div>}
            <Form>
                <Form.Item label="Ссылка на резюме интервью" required={true}>
                    <Input value={link} onChange={e => setLink(e.target.value)} />
                </Form.Item>
                <Button
                    onClick={handleSubmit(false)}
                    type="primary"
                    style={{ marginRight: '16px' }}
                >
                    Готовимся к консультации
                </Button>
                <Button onClick={handleSubmit(true)}>Нужно еще интервью</Button>
            </Form>
        </Card>
    )
}
