import { Descriptions } from 'antd'
import React from 'react'

import { ICCItems } from '../PageForm/PageForm'

export type ICC = {
    values: any
}

export const ICCData: React.FC<ICC> = ({ values }) => {
    return (
        <Descriptions column={1} bordered={true}>
            {ICCItems.map(item => {
                return (
                    <Descriptions.Item label={item.text} key={item.text}>
                        {values[item.path]}
                    </Descriptions.Item>
                )
            })}
        </Descriptions>
    )
}
