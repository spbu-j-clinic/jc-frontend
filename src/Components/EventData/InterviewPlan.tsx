import { Collapse } from 'antd'
import React from 'react'

import { SmartLink } from '../SmartLink/SmartLink'

export type InterviewPlan = {
    planLink: string
}

export const InterviewPlanData: React.FC<InterviewPlan> = ({ planLink }) => {
    return (
        <Collapse>
            <Collapse.Panel header={'План интервью'} key="plan">
                <SmartLink link={planLink} />
            </Collapse.Panel>
        </Collapse>
    )
}
