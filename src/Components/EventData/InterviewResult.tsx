import { Alert, Collapse } from 'antd'
import React from 'react'

import { SmartLink } from '../SmartLink/SmartLink'

export type ConsultPlan = {
    resultLink: string
    repeat: boolean
}

export const InterviewResultData: React.FC<ConsultPlan> = ({ resultLink, repeat }) => {
    return (
        <React.Fragment>
            {repeat && (
                <Alert
                    type="info"
                    message="Необходимо повторное интервью"
                    style={{ marginBottom: '10px' }}
                ></Alert>
            )}
            <Collapse>
                <Collapse.Panel header={'Резюме интервью'} key="plan">
                    <SmartLink link={resultLink} />
                </Collapse.Panel>
            </Collapse>
        </React.Fragment>
    )
}
