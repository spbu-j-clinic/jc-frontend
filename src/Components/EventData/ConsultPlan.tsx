import { Collapse } from 'antd'
import React from 'react'

import { SmartLink } from '../SmartLink/SmartLink'

export type ConsultPlan = {
    planLink: string
    memoLink: string
}

export const ConsultPlanData: React.FC<ConsultPlan> = ({ planLink, memoLink }) => {
    return (
        <React.Fragment>
            <Collapse>
                <Collapse.Panel header={'План консультации'} key="plan">
                    <SmartLink link={planLink} />
                </Collapse.Panel>
                <Collapse.Panel header={'Меморандум'} key="memo">
                    <SmartLink link={memoLink} />
                </Collapse.Panel>
            </Collapse>
        </React.Fragment>
    )
}
