import { Collapse, Descriptions } from 'antd'
import React from 'react'

import { SmartLink } from '../SmartLink/SmartLink'

export type Report = {
    reportLink: string
    resolution: 'done' | 'skip'
}

export const resolutionText = {
    done: 'Решено',
    skip: 'Не можем помочь',
}

export const ReportData: React.FC<Report> = ({ reportLink, resolution }) => {
    return (
        <React.Fragment>
            <Descriptions>
                <Descriptions.Item label={'Резолюция'}>
                    {resolutionText[resolution]}
                </Descriptions.Item>
            </Descriptions>
            <Collapse>
                <Collapse.Panel header={'Отчет по делу'} key="plan">
                    <SmartLink link={reportLink} />
                </Collapse.Panel>
            </Collapse>
        </React.Fragment>
    )
}
