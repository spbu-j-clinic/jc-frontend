import { Typography, Input, Form, Button } from 'antd'
import React from 'react'

import { Page } from '../Page/Page'
import { FormComponentProps } from 'antd/lib/form';
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const ADD_POINTS = gql`
    mutation AddPoints($id: Int!, $points: float8!) {
        insert_points_feed(objects: [{student_id: $id, points: $points}]) {
        affected_rows
        }
    }
`

const mainForm = (props: FormComponentProps) => {
    const { getFieldDecorator } = props.form;

    const formItemLayout = {
        labelCol: {
            xs: { span: 10 },
            sm: { span: 5 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };

    const normalizeFloat = (value: string) => parseFloat(value) || parseInt(value).toFixed(1) || 0.0;

    const [addPoints] = useMutation(ADD_POINTS);

    return (
        <Form {...formItemLayout} onSubmit={e => {
            e.preventDefault();
            props.form.validateFieldsAndScroll(async (err, values) => {
                if (!err) {
                    console.log('Received values of form: ', values);
                } else {
                    await addPoints({
                        variables: {
                            id: values.id,
                            points: values.points
                        }
                    })
                }
            });
        }
        }>
            <Form.Item label="ID студента">
                {getFieldDecorator('id', {
                    rules: [
                        {
                            transform: val => parseInt(val) || 0,
                            type: 'number',
                            message: 'Должно быть числом'
                        }, {
                            required: true,
                            message: 'Введите ID студента',
                        }],
                    normalize: val => parseInt(val) || 0
                })(<Input />)}
            </Form.Item>
            <Form.Item label="Количество баллов">
                {getFieldDecorator('points', {
                    rules: [{
                        transform: normalizeFloat,
                        type: 'float',
                        message: 'Должно быть числом'
                    },
                    {
                        required: true,
                        message: 'Введите ID студента',
                    }],
                    normalize: normalizeFloat
                })(<Input />)}
            </Form.Item>
            <Form.Item >
                <Button
                    type="primary"
                    size="large"
                    htmlType="submit"
                > Отправить
            </Button>
            </Form.Item>
        </Form >
    )
}

const FormBody = Form.create()(mainForm);

const SendPointsForm = () => {
    return (
        <Page>
            <Typography.Title>Отправить баллы</Typography.Title>
            <FormBody />
        </Page>
    )
}

export default SendPointsForm
