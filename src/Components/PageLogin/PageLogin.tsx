import React, { FormEvent } from 'react';
import { Form, Icon, Input, Button, message, Typography} from 'antd';
import { Page } from '../Page/Page';
import { Card } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { useRouter } from 'react-router5';

class NormalLoginForm extends React.Component<FormComponentProps> {

  handleSubmit(e:FormEvent) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) { console.log('Received values of form: ', values);
      message.loading('Идёт вход...',3.0);
      setTimeout(function() {
        const router = useRouter();
        router.navigate('me');
      }, 3000);
      } 
    });
    //return false;
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
    <Page>
     <Card>
      <Typography.Title>Юридическая клиника СПбГУ</Typography.Title> 
      <Typography.Title level={3}>Вход</Typography.Title>
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Пожалуйста, введите ваш логин!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Логин"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Пожалуйста, введите ваш пароль!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Пароль"
            />,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Вход &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
          </Button>
        </Form.Item>
      </Form>
     </Card>
    </Page>
    );
  }
}

const Login = Form.create({ name: 'normal_login' })(NormalLoginForm);

const PageLogin = () => {
  return (
      <Login />
  )
}


export default PageLogin;