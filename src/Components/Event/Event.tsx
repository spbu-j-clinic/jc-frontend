import { useMutation } from '@apollo/react-hooks'
import { Alert, Card, message } from 'antd'
import gql from 'graphql-tag'
import React from 'react'

import { DateView } from '../Date/Date'
import { CheckCard } from '../EventActions/Check'
import { CloseCard } from '../EventActions/Close'
import { ConsultPlanCard } from '../EventActions/SubmitConsultationPlan'
import { ConsultResultCard } from '../EventActions/SubmitConsultationResult'
import { ICCCard } from '../EventActions/SubmitICC'
import { InterviewPlanCard } from '../EventActions/SubmitInterviewPlan'
import { InterviewResultCard } from '../EventActions/SubmitInterviewResult'
import { ConsultPlanData } from '../EventData/ConsultPlan'
import { ConsultResultData } from '../EventData/ConsultResult'
import { ICCData } from '../EventData/ICC'
import { InterviewPlanData } from '../EventData/InterviewPlan'
import { InterviewResultData } from '../EventData/InterviewResult'
import { ReportData } from '../EventData/Report'
import { useAppState } from '../state'
import styles from './Event.less'

export type EventProps = {
    event_id: number
    type: string
    case_id: number
    date: string | number
    done: boolean
    titlePrefix: string
    eventData: any
}

const ADD_EVENT = gql`
    mutation AddEvent($id: Int!, $type: name!, $data: json!, $evId: Int!) {
        insert_cases_feed(objects: [{ case_id: $id, case_status: $type, event_data: $data }]) {
            affected_rows
        }
        update_cases_feed(where: { event_id: { _eq: $evId } }, _set: { done: true }) {
            affected_rows
        }
    }
`

const getView = (
    props: EventProps,
    roles: string[],
    addEvent: (gqlArgs: { variables: { id: number; type: string; data: any; evId: number } }) => any
) => {
    // ICC
    if (props.type === 'icc_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <ICCCard
                    onSubmit={(values: any) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'icc_review',
                                data: JSON.stringify({ values }),
                                evId: props.event_id,
                            },
                        })
                    }
                    values={props.eventData.values}
                    title={`${props.titlePrefix}ИКК`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </ICCCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получен' : 'Ждем'
                        } исправленный ИКК от диспетчера, причина "${props.eventData.reason}"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${props.done ? 'Получен' : 'Ждем'} ИКК  от диспетчера`}
                    />
                )
            }
        }
    }
    if (props.type === 'icc_review') {
        const base = <ICCData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_plan_request',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'icc_request',
                                data: JSON.stringify({ ...props.eventData, reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}План интервью`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`План интервью ${
                            props.done ? 'проверен' : 'ожидает подтверждения'
                        } тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    // Interview plan
    if (props.type === 'interview_plan_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <InterviewPlanCard
                    onSubmit={(link: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_plan_review',
                                data: JSON.stringify({ planLink: link }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}План интервью`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </InterviewPlanCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получен' : 'Ждем'
                        } исправленный план интервью от консультанта, причина "${
                            props.eventData.reason
                        }"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${props.done ? 'Получен' : 'Ждем'} план интервью от консультанта`}
                    />
                )
            }
        }
    }

    if (props.type === 'interview_plan_review') {
        const base = <InterviewPlanData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_result_request',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_plan_request',
                                data: JSON.stringify({ reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}План интервью`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`План интервью ${
                            props.done ? 'проверен' : 'ожидает подтверждения'
                        } тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    // Interview result
    if (props.type === 'interview_result_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <InterviewResultCard
                    onSubmit={link =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_result_review',
                                data: JSON.stringify({ resultLink: link }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Резюме интервью`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </InterviewResultCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получено' : 'Ждем'
                        } исправленное резюме интервью от консультанта, причина "${
                            props.eventData.reason
                        }"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получено' : 'Ждем'
                        } резюме интервью от консультанта`}
                    />
                )
            }
        }
    }

    if (props.type === 'interview_result_review') {
        const base = <InterviewResultData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_plan_request',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'interview_result_request',
                                data: JSON.stringify({ reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Резюме интервью`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`Резюме интервью ${
                            props.done ? 'проверено' : 'ожидает подтверждения'
                        }' тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    // Consultation plan
    if (props.type === 'consultation_plan_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <ConsultPlanCard
                    onSubmit={(planLink: string, memoLink: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_plan_review',
                                data: JSON.stringify({ planLink, memoLink }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}План консультации`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </ConsultPlanCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получен' : 'Ждем'
                        } исправленный план консультации от консультанта, причина "${
                            props.eventData.reason
                        }"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получен' : 'Ждем'
                        } план консультации от консультанта`}
                    />
                )
            }
        }
    }

    if (props.type === 'consultation_plan_review') {
        const base = <ConsultPlanData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_result_request',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_plan_request',
                                data: JSON.stringify({ reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}План консультации`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`План консультации ${
                            props.done ? 'проверен' : 'ожидает подтверждения'
                        } тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    if (props.type === 'consultation_result_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <ConsultResultCard
                    onSubmit={link =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_result_review',
                                data: JSON.stringify({ resultLink: link }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Резюме консультации`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </ConsultResultCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получено' : 'Ждем'
                        } исправленное резюме консультации от консультанта, причина "${
                            props.eventData.reason
                        }"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получено' : 'Ждем'
                        } резюме консультации от консультанта`}
                    />
                )
            }
        }
    }

    if (props.type === 'consultation_result_review') {
        const base = <ConsultResultData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'report_request',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'consultation_result_request',
                                data: JSON.stringify({ reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Резюме консультации`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`Резюме консультации ${
                            props.done ? 'проверено' : 'ожидает подтверждения'
                        } тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    // Report
    if (props.type === 'report_request') {
        if (roles.includes('student') && !props.done) {
            return (
                <CloseCard
                    onSubmit={(reportLink: string, resolution: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'report_review',
                                data: JSON.stringify({ report: reportLink, resolution }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Отчет по делу`}
                >
                    {props.eventData.reason && (
                        <Alert type="error" message={props.eventData.reason} />
                    )}
                </CloseCard>
            )
        } else {
            if (props.eventData.reason) {
                return (
                    <Alert
                        type="info"
                        message={`${
                            props.done ? 'Получен' : 'Ждем'
                        } исправленный отчет по делу от консультанта, причина "${
                            props.eventData.reason
                        }"`}
                    />
                )
            } else {
                return (
                    <Alert
                        type="info"
                        message={`${props.done ? 'Получен' : 'Ждем'} отчет по делу от консультанта`}
                    />
                )
            }
        }
    }

    if (props.type === 'report_review') {
        const base = <ReportData {...props.eventData} />

        if (roles.includes('tutor') && !props.done) {
            return (
                <CheckCard
                    onAccept={() =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'close',
                                data: JSON.stringify({}),
                                evId: props.event_id,
                            },
                        })
                    }
                    onReject={(reason: string) =>
                        addEvent({
                            variables: {
                                id: props.case_id,
                                type: 'report_request',
                                data: JSON.stringify({ reason }),
                                evId: props.event_id,
                            },
                        })
                    }
                    title={`${props.titlePrefix}Отчет по делу`}
                >
                    {base}
                </CheckCard>
            )
        } else {
            return (
                <Card>
                    <Alert
                        type="info"
                        message={`Отчет по делу ${
                            props.done ? 'проверен' : 'ожидает подтверждения'
                        } тьютором`}
                        className={styles.meta}
                    />
                    {base}
                </Card>
            )
        }
    }

    return null
}

export const EventView: React.FC<EventProps> = props => {
    const state = useAppState()
    const [addEvent] = useMutation(ADD_EVENT)

    const add = async (...args: any[]) => {
        message.loading('UPD')
        await addEvent(...args)
        message.destroy()
        message.success('Обновлено', 1)
    }

    if (props.type === 'start') {
        return (
            <React.Fragment>
                <DateView date={props.date} />
                <Alert type="info" message="Дело начато"></Alert>
            </React.Fragment>
        )
    }

    if (props.type === 'close') {
        return (
            <React.Fragment>
                <DateView date={props.date} />
                <Alert type="success" message="Дело закрыто"></Alert>
            </React.Fragment>
        )
    }

    const newProps = { ...props }

    if (typeof props.eventData === 'string') {
        newProps.eventData = JSON.parse(props.eventData)
    }

    const view: React.ReactNode = getView(newProps, state.userRoles, add)

    return (
        <React.Fragment>
            <DateView date={props.date} />
            {view || (
                <Card>
                    <pre>{JSON.stringify(newProps, null, 4)}</pre>
                </Card>
            )}
        </React.Fragment>
    )
}
