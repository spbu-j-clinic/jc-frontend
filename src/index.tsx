import day from 'dayjs'
import ru from 'dayjs/locale/ru'
import relativeTime from 'dayjs/plugin/relativeTime'
import React from 'react'
import ReactDOM from 'react-dom'

import { App } from './Components/App/App'

day.extend(relativeTime)
day.locale(ru)

const root = document.createElement('div')

root.id = 'root'
document.body.appendChild(root)

ReactDOM.render(<App />, root)
