export const sortBy = <T, F extends keyof T>(field: F, compare: (a: T[F], b: T[F]) => number) => (
    a: T,
    b: T
) => compare(a[field], b[field])

export const byNumber = (a: number, b: number) => a - b
export const lexical = (a: string, b: string) => a.localeCompare(b)
