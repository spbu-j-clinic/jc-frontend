import { ColumnProps, SorterResult } from 'antd/lib/table'
import { useCallback, useMemo, useState } from 'react'

export const useSortedColumns = <T>(columns: ColumnProps<T>[]) => {
    const [sorter, setSorter] = useState<SorterResult<T> | null>(null)
    const handleChange = useCallback((_: unknown, _1: unknown, newSorter: SorterResult<T>) => {
        setSorter(newSorter)
    }, [])

    const sortedColumns: ColumnProps<T>[] = useMemo(
        () =>
            columns.map(col => ({
                ...col,
                sortOrder: sorter && sorter.columnKey === col.key ? sorter.order : false,
            })),
        [columns, sorter]
    )

    return [sortedColumns, handleChange] as const
}
