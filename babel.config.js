module.exports = api => {
    const presets = [
        '@babel/preset-react',
        '@babel/preset-typescript',
        ['@babel/preset-env', { modules: api.env('test') ? 'cjs' : false }],
    ]
    const plugins = [['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }]]

    return {
        presets,
        plugins,
    }
}
