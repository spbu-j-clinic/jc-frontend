const merge = require('webpack-merge')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const common = require('./webpack.config')

module.exports = env =>
    merge(common(env), {
        mode: 'production',
        devtool: 'source-map',
        devServer: undefined,
        output: {
            filename: '[name].[contenthash].js',
            chunkFilename: '[name].[contenthash].js',
        },
        plugins: [new CleanWebpackPlugin(), ...(env && env.a ? [new BundleAnalyzerPlugin()] : [])],
    })
