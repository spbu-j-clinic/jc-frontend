declare module '*.less' {
    const content: {
        [style: string]: string
    }

    export default content
}
