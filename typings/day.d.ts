declare module 'dayjs/locale/*' {
    const locale: string

    export default locale
}
